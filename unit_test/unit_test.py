import sys
import unittest

sys.path.append('./src')

from FizzBuzz import FizzBuzz



default_output = ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz', '11', 'Fizz', '13', '14', 'FizzBuzz', '16', '17', 'Fizz', '19', 'Buzz', 'Fizz', '22', '23', 'Fizz', 'Buzz', '26', 'Fizz', '28', '29', 'FizzBuzz', '31', '32', 'Fizz', '34', 'Buzz', 'Fizz', '37', '38', 'Fizz', 'Buzz', '41', 'Fizz', '43', '44', 'FizzBuzz', '46', '47', 'Fizz', '49', 'Buzz', 'Fizz', '52', '53', 'Fizz', 'Buzz', '56', 'Fizz', '58', '59', 'FizzBuzz', '61', '62', 'Fizz', '64', 'Buzz', 'Fizz', '67', '68', 'Fizz', 'Buzz', '71', 'Fizz', '73', '74', 'FizzBuzz', '76', '77', 'Fizz', '79', 'Buzz', 'Fizz', '82', '83', 'Fizz', 'Buzz', '86', 'Fizz', '88', '89', 'FizzBuzz', '91', '92', 'Fizz', '94', 'Buzz', 'Fizz', '97', '98', 'Fizz']


class TestFizzBuzz(unittest.TestCase):
    def test_simple_of_naive(self):
        self.assertEqual(FizzBuzz.naive(), default_output)

    def test_simple_of_extendable(self):
        self.assertEqual(FizzBuzz.extendable(), default_output)
    
    def test_the_equality_of_both_function(self):
        self.assertEqual(FizzBuzz.extendable(), FizzBuzz.naive())
    
    def test_output_length_for_non_default(self):
        self.assertEqual(9, len(FizzBuzz.extendable(1, 10)))
    
    def test_output_length_for_default(self):
        self.assertEqual(99, len(FizzBuzz.extendable()))
        
    def test_the_output_for_non_default_end(self):
        self.assertEqual(default_output[0:49], FizzBuzz.extendable(1, 50))
    
    def test_the_output_for_non_default_start(self):
        self.assertEqual(default_output[9:], FizzBuzz.extendable(10))
    
    def test_the_output_for_non_default(self):
        self.assertEqual(default_output[5:49], FizzBuzz.extendable(6, 50))
    
    def test_out_of_range_num(self):
        self.assertEqual(['101'], FizzBuzz.extendable(101, 102))
    
    def test_out_of_range_fizz(self):
        self.assertEqual(['Fizz'], FizzBuzz.extendable(102, 103))
        
    def test_out_of_range_buzz(self):
        self.assertEqual(['Buzz'], FizzBuzz.extendable(110, 111))
        
    def test_out_of_range_fizzbuzz(self):
        self.assertEqual(['FizzBuzz'], FizzBuzz.extendable(105, 106))


if __name__ == '__main__':
    unittest.main()

