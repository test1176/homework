class FizzBuzz():
    def __init__(self):
        pass

    @staticmethod
    def naive(start=1, end=100):
        ret = list()
        for i in range(start, end):
            if i % 15 == 0:
                ret.append('FizzBuzz')
            elif i % 3 == 0:
                ret.append('Fizz')
            elif i % 5 == 0:
                ret.append('Buzz')
            else:
                ret.append(str(i))
        return ret

    @staticmethod
    def extendable(start=1, end=100):
        message = ['', 'Fizz', 'Buzz', 'FizzBuzz']
        ret = []
        for i in range(start, end):
            tester=0
            if i % 3 == 0:
                tester |= 1
            if i % 5 == 0:
                tester |= 2
            message[0] = str(i)
            ret.append(message[tester])
        return ret
 
print(FizzBuzz.naive())
print(FizzBuzz.extendable())
